/*
    Christopher Mahon
    Created 20/8/2018

*/
#include <iostream>
#include <string>
#include "headers/rsa.hpp"
#include "headers/utils.hpp"

using namespace std;

int main()
{
    int input = 112462;
    RSA instance;
    instance.show_keys();
    instance.decrypt(instance.encrypt(input));
    return 0;
}