"""Christopher Mahon - Created 16 Mar 2015
"""

from random import getrandbits, randint 
from sys import maxsize, exit
from tkinter import *
from fractions import gcd
from time import sleep
from math import sqrt
import datetime


class RSA:
	FirstPrime = 0
	SecondPrime = 0 
	
	def __init__(self):
		self.PrimeGeneration()

		self.publickey = 0
		self.privatekey = 0
		self.p = self.FirstPrime * self.SecondPrime
		pn = (self.FirstPrime-1) * (self.SecondPrime-1)
		
		e=150
		while(self.gcd(e, pn)!=1):
			e+=1
			
		d = 2
		while (d * e)%pn != 1:
			d += 1
				
		self.publickey = e
		self.privatekey = d
	
	def RandomKeyGen(self):
		random = randint(100, 500)
		truth = self._PrimeCheck(random)
		
		if (random < 100):
			self.RandomKeyGen()
		elif truth != 0:
			self.RandomKeyGen()
			
		return random
		
	def _PrimeCheck(self, maxnum):
		n = 1
		if (maxnum%2 == 0):
			return 1
		elif(maxnum%3 == 0):
			return 1
		elif(maxnum%5 == 0):
			return 1
		while(maxnum % n != 0 and n <int(sqrt(maxnum)+1)):
			if (maxnum%n == 0):
				return 1
			n = n + 1

		return 0
		
	def PrimeGeneration(self):
		self.FirstPrime = self.RandomKeyGen()
		self.SecondPrime = self.RandomKeyGen()
		
	def gcd(self, ae, bpn):
		while bpn > 0:
			ae, bpn = bpn, ae % bpn
		return ae
	
	def Encrypt(self, m):
		print("p {}".format(self.p))
		return ((m**self.publickey) % self.p)
		
	def Decrypt(self, c):
		return ((c**self.privatekey) % self.p)

def __exit():
	root.destroy()
	exit()
	
def encrypt():
	String = ""
	m = Input.get(1.0, END)
	for i in m:
		print(i)
		if (type(i) != int):
			i = ord(i)
		String+= str(Encryption.Encrypt(i)) + " "
	
	print(String)
	Out.config(text = String)
	
def decrypt():
	String = ""
	l = []
	c = Input.get(1.0, END)
	data = c.split()
	for i in range(len(data)):
		print(data[i])
		if (type(data[i]) != int):
			data[i] = int(data[i])
		l.append(Encryption.Decrypt(data[i]))
	print(l)
	Out.config(text = String)
		
c = ""
Encryption = RSA()
root = Tk()

B1 = Button(root, text ="Close me!",width= 20, height=8, command = __exit)
B1.grid(row=0, column=0)

B2 = Button(root, text ="Encrypt!",width= 20, height=8, command = lambda:encrypt())
B2.grid(row=1, column=0)
B3 = Button(root, text ="Decrypt!",width= 20, height=8, command = lambda:decrypt())
B3.grid(row=1, column=1)

L1 = Label(root, text="Public Key: " + str(Encryption.publickey), width= 20, height=8)
L1.grid(row=0, column = 1)

L2 = Label(root, width= 20, height=8, text = "Private Key: "+ str(Encryption.privatekey))
L2.grid(row=0, column = 2)

Input= Text(root, width=20, height=4)
Input.grid(row=2, column=0)

Out = Label(root, width= 30, height=8, text = c, wraplength=200)
Out.grid(row=2, column = 1)



root.mainloop()