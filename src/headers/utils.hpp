#pragma once

#include <math.h>
#include <cstdlib>

// Arbitrary Limits for demonstration purposes
#define MAXRAND 300
#define MINRAND 150

bool isPrime(int);
int generatePrime();
int gcd(int, int);
int coprime(int);