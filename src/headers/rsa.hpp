#pragma once

#include <iostream>
#include <string>
#include <ctime>
#include <math.h>
#include "utils.hpp"

using namespace std;

class RSA
{
    public:
        RSA();
        ~RSA();
        int encrypt(int);
        int decrypt(int);
        int const public_key() { return pub_key; }
        void const show_keys(); //Normally this would not be used
    private:
        int priv_key, pub_key, p;
};