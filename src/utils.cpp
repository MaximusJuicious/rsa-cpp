#include <iostream>

#include "headers/utils.hpp"

bool isPrime(int number)
{
    for (int i = 2; i<=number/2; i++)
    {
        //printf("%d %d %d\n", i, number, number%i);
        if (int(number%i)==0)
            return false;
    }
    return true;
}

int generatePrime()
{
    int result = rand() % MAXRAND;
    if (!isPrime(result) || result < MINRAND)
    {
        result = generatePrime();
    }

    return result;
}

// Greatest Common Denominator
int gcd(int number1, int number2)
{
    int x, y, z;
    x = number1;
    y = number2;

    while (y > 0)
    {
        z = x;
        x = y;
        y = z%y;

    }
    return x;

}

int coprime(int number)
{
    int g = 0;
    int result=150;

    while (g != 1)
    {
        //result = rand() % number;
        g = gcd(result, number);
        result++;
    }
    return result;
}
