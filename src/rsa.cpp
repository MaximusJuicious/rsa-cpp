#include "headers/rsa.hpp"

RSA::RSA()
{
    srand(time(NULL));
    int pn, d;
    int prime1 = generatePrime();
    int prime2 = generatePrime();

    p = prime1 * prime2;

    pn = (prime1 - 1) * (prime2 - 1);

    priv_key = coprime(pn);

    d = 2;
    while (((d*priv_key)%pn) != 0)
    {
        d++;
    }
    pub_key = d;
}

RSA::~RSA()
{
    // Setting public and private keys to zero to remove them from memory
    pub_key = 0;
    priv_key = 0;
}

void const RSA::show_keys()
{ 
    printf("Private Key:%d Public Key:%d p:%d\n", priv_key, pub_key, p);
}

int RSA::encrypt(int message)
{
    unsigned long cipher = 0;
    cipher = (unsigned long)pow(message, priv_key) % p;
    cout << "Encrypted:" << cipher << " power:" << pow(message, priv_key) << endl;
    return cipher;
}

int RSA::decrypt(int cipher)
{
    unsigned long message = 0;
    message = (unsigned long)pow(cipher, pub_key) % p;

    cout << "Decrypted:" << message  << " power:" << pow(message, priv_key) << endl;
    return message;
}