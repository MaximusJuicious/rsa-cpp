#OBJS specifies which files to compile as part of the project
SOURCE_DIR = src
BINARY_DIR = bin

#List of c++ files
SOURCES =  $(wildcard $(SOURCE_DIR)/*.cpp)

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = rsa

CC = g++

PLATFORM_INCLUDE =
PLATFORM_LIBRARY =
PLATFORM_COMPILER_FLAGS =
PLATFORM_LINKER_FLAGS =

OBJS = $(SOURCES:.cpp=.o)

COMPILER_FLAGS = $(PLATFORM_COMPILER_FLAGS) -w

LINKER_FLAGS = $(PLATFORM_LINKER_FLAGS) -lm

INCLUDE_PATHS = $(PLATFORM_INCLUDE)
LIBRARY_PATHS = $(PLATFORM_LIBRARY)

#This is the target that compiles our exeCutable
all : clean $(MAIN)
	$(CC) $(SOURCES) $(LIBRARY_PATHS) $(INCLUDE_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(BINARY_DIR)/$(OBJ_NAME)

clean: 
	rm -rf bin/*.out
	rm -rf src/*.o
	
